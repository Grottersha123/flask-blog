from wtforms import Form, StringField, TextAreaField
from wtforms.validators import InputRequired


class PostsForm(Form):
    title = StringField('Title', [InputRequired()])
    body = TextAreaField('Body', [InputRequired()])