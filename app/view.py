from flask import render_template

from app import app


@app.route('/')
def index():
    n = 'Steven'
    return render_template('index.html', n='Steven')
